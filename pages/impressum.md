.. title: Impressum
.. slug: impressum
.. date: 1970-01-01 00:00:00 UTC
.. hidetitle: True
.. tags:
.. link:
.. description:

# Impressum / Kontakt

<p>Verantwortlich für den Inhalt dieser Website ist <i>FunkFeuer Wien - Verein zur Förderung freier Netze</i>, ZVR-Zahl 814804682.</p>

<br/>
**Anfragen betreffend Housing:** housing-ticket(at)funkfeuer.at

**Kontaktanfragen via E-Mail:** vorstand(at)funkfeuer.at

**Presseanfragen bitte an:** presse(at)funkfeuer.at

**Rechnungen, Buchhaltung bitte an:** billing(at)funkfeuer.at

<hr/>

**Adresse:**
<pre>
FunkFeuer Wien - Verein zur Förderung freier Netze
c/o Volkskundemuseum Wien
Laudongasse 15-19
1080 Wien
</pre>


**Bankverbindung:**
<pre>
FunkFeuer Wien - Verein zur Förderung freier Netze
 IBAN: AT55 2023 0000 0014 3982
 BIC/SWIFT: SPLSAT21 (Sparkasse Langenlois)
</pre>
