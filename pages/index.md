.. title: Home
.. slug: index
.. date: 1970-01-01 00:00:00 UTC
.. hidetitle: True
.. tags:
.. link:
.. description:
.. template: index_page.tmpl


# Idee

<img src="/files/switch.jpg" style="margin: 15px 15px 15px 0px; float:left; " class="img-polaroid">

Erst durch das Serverhousing kann der Verein FunkFeuer Wien die für den Betrieb des Funknetzes notwendigen Ressourcen (Bandbreite und Strom) aufbringen und betreiben, sowie die Idee des Vereins (Er- und Beforschung freier Netzwerke) in der Praxis bei kabelgebundenen Netzen verwirklichen.

FunkFeuer Wien ist dank Serverhousing Local Internet Registry (LIR), verwaltet öffentliche IPv6- und IPv4-Adressen, ist multi-homed angebunden und betreibt eigene technische Dienste (DNS-Server, administrative Server, Mailinglisten usw.) im Serverhousing, die für den Betrieb des Funknetzes unabdingbar sind.

> Das Serverhousing stellt Internetuplink für das experimentelle Funknetz zur Verfügung.

<img src="/files/raum.jpg" style="margin: 15px 0px 15px 15px; float: right" class="img-polaroid">

Wir laden Privatpersonen, Netzkultur-Communitys, Vereine usw. ein, bei uns zu einem Unkostenbeitrag einen eigenen Server im Serverhousing zu betreiben, der dann an einem hochwertigen Internetuplink hängt.

Für jeden eingestellten Server wird ein Betriebskostenbeitrag an "housing.funkfeuer.at" entrichtet.
Damit wird es möglich, gemeinschaftlich die Kosten für Strom, Bandbreite und Infrastruktur zu decken. Jeder eingestellte Server unterstützt das Funknetz.

Durch gemeinsame freiwillige Arbeit werden laufend anfallende Arbeiten erledigt. Es gibt keinen offiziellen Support sowie diverse Einschränkungen, z.B. Zutritt nur nach Absprache mit einem Admin, und keine Verfügbarkeitsgarantien.


# Eckdaten

## Anbindung

- Anbindung des Servers an Fast- oder Gigabit-Ethernet-Switch
- Uplink über Funk, Gigabit-Ethernet-Netzwerk zu den Upstream-Partnern
- Internetzugang Bandbreite Spitzen bis 1GBit/s möglich (*)
- Backbone-Anbindung multi-homed über 3 Upstream-Partner

## mögliche Serverformate

- Mini/Midi/Big-Tower
- 19" Rack-Housing
- Mini-ITX & Co.
- Embedded-Board (8W / 5V microUSB)

## Unkostenbeitrag

- €40/Monat Standard (150W, 350GB fair-use)
- €19/Monat Micro (geringer Strom- und Bandbreitenverbrauch, 45W, 100GB fair-use)
- €5/Monat Embedded-Board (8W/5V=1.6A, 100GB)
- €6/Monat Stromupgrade pro 50W (für Standard)
- €5/Monat Traffic-Upgrade pro 250GB

## Anmeldung

Bei Interesse zögern Sie nicht, uns persönlich unter `housing-ticket [at] funkfeuer [punkt] at` zu kontaktieren.

- [Allgemeine Geschäftsbedingungen](/files/AGB.pdf)
- [SEPA-Lastschrift-Mandat](/files/SEPA-Lastschrift.pdf)
- [Muster: Nutzungsvertrag Serverplatz](/files/Serverplatz.pdf)
- [Muster: Nutzungsvertrag Micro-Serverplatz](/files/Micro-Serverplatz.pdf)
- [Muster: Nutzungsvertrag Embedded-Serverplatz](/files/Embedded-Serverplatz.pdf)
- [Muster: Stromupgrade](/files/Stromupgrade.pdf)
